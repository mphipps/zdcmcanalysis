# Project Goal:

Facilitate the analysis and validation of the new ZDC MC framework. This will be done through a number of methods, including: a response comparison of N neutron scenarios, data vs mc, A-side vs C-side, energy type deposition, and deposition as a function of module.

# To Use:

The first analysis script in this project is called src/mcAnalysis.C. To compile, simply type "make mcAnalysis" from the working directory. Then to run, type  "./obj/mcAnalysis.o". Results will appear in Output/png/. Input ntuples can be found in the Input directory. An NtupleReader class is invoked in utils/NtupleReader*. The utils folder also includes the histogram style files used by ATLAS.

When new analysis scripts are created put them in the src folder and update the Makefile to include the new target.

## Before using the first time:

Copy MC ntuples from /eos/atlas/atlascerngroupdisk/det-zdc/G4neutronsN1-8 to Input/MC/, and copy data ntuples from /eos/atlas/atlascerngroupdisk/det-zdc/zdcCalib_70z to Input/Data/.  These data nutples come from the last major 2015 processing drive. 

# Simulation details:

For more details on the simulation ntuples (including energy classification, truth container mapping and ntuple formatting), see: https://indico.cern.ch/event/835671/contributions/3503152/attachments/1888321/3113487/ZDC_Simulation_Ntuples_07302019.pdf. And then details on the geant implementation can be found here: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ZdcSimulation.

# Git Interface:

## Update the master with your changes:
git checkout master  
git add .  
git commit -m "message"  
git push --set-upstream origin master  

## Update the master with someone else's changes:
git checkout master  
git pull origin master  

## Merge branch with master:
git checkout branch1  
git merge master -m "message"  
git push --set-upstream branch1  

# Git Cheatsheet

### see help page for any command  
git commandName --help

### see differences between working directory and index
git diff

### see differences between two different commits
git diff commit1 commit2

### see current branch and changes to be committed
git status

### see list of branches and current branch
git branch

### see more detailed branch info
git show-branch

### create branch off HEAD of current branch
git branch newBranchName

### create branch off arbitrary commit
git branch newBranchName startingPointCommit

### see log files
git log

### remove file from git
git rm fileName

### move or rename file on git
git mv oldFileName newFileName

