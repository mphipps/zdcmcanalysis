//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr 15 16:58:09 2020 by ROOT version 6.14/04
// from TTree zdcTree/ZDC Tree
// found on file: user.mphipps.PG_10000Neutrons_1N_2.51TeV_80MHz_try3.20200217.1551_EXT3.root
//////////////////////////////////////////////////////////

#ifndef NtupleReader_mc_h
#define NtupleReader_mc_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class NtupleReader_mc {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   UInt_t          eventNumber;
   UInt_t          lumiBlock;
   UInt_t          bcid;
   Float_t         avgIntPerCrossing;
   Float_t         actIntPerCrossing;
   ULong64_t       trigger;
   UInt_t          trigger_TBP;
   UInt_t          tbp[16];
   UInt_t          tav[16];
   UInt_t          passBits;
   Float_t         zdc_amp_rp[2][4];
   Float_t         zdc_time[2][4];
   Float_t         zdc_ampHG_rp[2][4];
   Float_t         zdc_ampLG_rp[2][4];
   Float_t         zdc_sumHG_rp[2];
   Float_t         zdc_sumLG_rp[2];
   Float_t         zdc_ZdcAmp[2];
   Float_t         zdc_ZdcAmpErr[2];
   Float_t         zdc_ZdcEnergy[2];
   Float_t         zdc_ZdcEnergyErr[2];
   Float_t         zdc_ZdcTime[2];
   Short_t         zdc_ZdcStatus[2];
   Float_t         zdc_ZdcTrigEff[2];
   UInt_t          zdc_ZdcModuleMask;
   Float_t         zdc_VisTruthEnergy[2][4][2][650];
   Float_t         zdc_EMTruthEnergy[2][4][2][650];
   Float_t         zdc_TotalTruthEnergy[2][4][2][650];
   Float_t         zdc_NonEMTruthEnergy[2][4][2][650];
   Float_t         zdc_VisTruthEnergySum[2][4][2];
   Float_t         zdc_EMTruthEnergySum[2][4][2];
   Float_t         zdc_TotalTruthEnergySum[2][4][2];
   Float_t         zdc_NonEMTruthEnergySum[2][4][2];
   Float_t         zdc_ZdcModuleAmp[2][4];
   Float_t         zdc_ZdcModuleTime[2][4];
   Float_t         zdc_ZdcModuleFitAmp[2][4];
   Float_t         zdc_ZdcModuleFitT0[2][4];
   UInt_t          zdc_ZdcModuleStatus[2][4];
   Float_t         zdc_ZdcModuleChisq[2][4];
   Float_t         zdc_ZdcModuleCalibAmp[2][4];
   Float_t         zdc_ZdcModuleCalibTime[2][4];
   Float_t         zdc_ZdcModuleBkgdMaxFraction[2][4];
   Float_t         zdc_ZdcModuleAmpError[2][4];
   Float_t         zdc_ZdcModuleMinDeriv2nd[2][4];
   Float_t         zdc_ZdcModulePresample[2][4];
   Float_t         zdc_ZdcModulePreSampleAmp[2][4];

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_avgIntPerCrossing;   //!
   TBranch        *b_actIntPerCrossing;   //!
   TBranch        *b_trigger;   //!
   TBranch        *b_trigger_TBP;   //!
   TBranch        *b_tbp;   //!
   TBranch        *b_tav;   //!
   TBranch        *b_passBits;   //!
   TBranch        *b_zdc_amp_rp;   //!
   TBranch        *b_zdc_time;   //!
   TBranch        *b_zdc_ampHG_rp;   //!
   TBranch        *b_zdc_ampLG_rp;   //!
   TBranch        *b_zdc_sumHG_rp;   //!
   TBranch        *b_zdc_sumLG_rp;   //!
   TBranch        *b_zdc_ZdcAmp;   //!
   TBranch        *b_zdc_ZdcAmpErr;   //!
   TBranch        *b_zdc_ZdcEnergy;   //!
   TBranch        *b_zdc_ZdcEnergyErr;   //!
   TBranch        *b_zdc_ZdcTime;   //!
   TBranch        *b_zdc_ZdcStatus;   //!
   TBranch        *b_zdc_ZdcTrigEff;   //!
   TBranch        *b_zdc_ZdcModuleMask;   //!
   TBranch        *b_zdc_VisTruthEnergy;   //!
   TBranch        *b_zdc_EMTruthEnergy;   //!
   TBranch        *b_zdc_TotalTruthEnergy;   //!
   TBranch        *b_zdc_NonEMTruthEnergy;   //!
   TBranch        *b_zdc_VisTruthEnergySum;   //!
   TBranch        *b_zdc_EMTruthEnergySum;   //!
   TBranch        *b_zdc_TotalTruthEnergySum;   //!
   TBranch        *b_zdc_NonEMTruthEnergySum;   //!
   TBranch        *b_zdc_ZdcModuleAmp;   //!
   TBranch        *b_zdc_ZdcModuleTime;   //!
   TBranch        *b_zdc_ZdcModuleFitAmp;   //!
   TBranch        *b_zdc_ZdcModuleFitT0;   //!
   TBranch        *b_zdc_ZdcModuleStatus;   //!
   TBranch        *b_zdc_ZdcModuleChisq;   //!
   TBranch        *b_zdc_ZdcModuleCalibAmp;   //!
   TBranch        *b_zdc_ZdcModuleCalibTime;   //!
   TBranch        *b_zdc_ZdcModuleBkgdMaxFraction;   //!
   TBranch        *b_zdc_ZdcModuleAmpError;   //!
   TBranch        *b_zdc_ZdcModuleMinDeriv2nd;   //!
   TBranch        *b_zdc_ZdcModulePresample;   //!
   TBranch        *b_zdc_ZdcModulePreSamplemp;   //!

   NtupleReader_mc();
   virtual ~NtupleReader_mc();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef NtupleReader_mc_cxx
NtupleReader_mc::NtupleReader_mc() : fChain(0) 
{;}

NtupleReader_mc::~NtupleReader_mc()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t NtupleReader_mc::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t NtupleReader_mc::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void NtupleReader_mc::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("avgIntPerCrossing", &avgIntPerCrossing, &b_avgIntPerCrossing);
   fChain->SetBranchAddress("actIntPerCrossing", &actIntPerCrossing, &b_actIntPerCrossing);
   fChain->SetBranchAddress("trigger", &trigger, &b_trigger);
   fChain->SetBranchAddress("trigger_TBP", &trigger_TBP, &b_trigger_TBP);
   fChain->SetBranchAddress("tbp", tbp, &b_tbp);
   fChain->SetBranchAddress("tav", tav, &b_tav);
   fChain->SetBranchAddress("passBits", &passBits, &b_passBits);
   fChain->SetBranchAddress("zdc_amp_rp", zdc_amp_rp, &b_zdc_amp_rp);
   fChain->SetBranchAddress("zdc_time", zdc_time, &b_zdc_time);
   fChain->SetBranchAddress("zdc_ampHG_rp", zdc_ampHG_rp, &b_zdc_ampHG_rp);
   fChain->SetBranchAddress("zdc_ampLG_rp", zdc_ampLG_rp, &b_zdc_ampLG_rp);
   fChain->SetBranchAddress("zdc_sumHG_rp", zdc_sumHG_rp, &b_zdc_sumHG_rp);
   fChain->SetBranchAddress("zdc_sumLG_rp", zdc_sumLG_rp, &b_zdc_sumLG_rp);
   fChain->SetBranchAddress("zdc_ZdcAmp", zdc_ZdcAmp, &b_zdc_ZdcAmp);
   fChain->SetBranchAddress("zdc_ZdcAmpErr", zdc_ZdcAmpErr, &b_zdc_ZdcAmpErr);
   fChain->SetBranchAddress("zdc_ZdcEnergy", zdc_ZdcEnergy, &b_zdc_ZdcEnergy);
   fChain->SetBranchAddress("zdc_ZdcEnergyErr", zdc_ZdcEnergyErr, &b_zdc_ZdcEnergyErr);
   fChain->SetBranchAddress("zdc_ZdcTime", zdc_ZdcTime, &b_zdc_ZdcTime);
   fChain->SetBranchAddress("zdc_ZdcStatus", zdc_ZdcStatus, &b_zdc_ZdcStatus);
   fChain->SetBranchAddress("zdc_ZdcTrigEff", zdc_ZdcTrigEff, &b_zdc_ZdcTrigEff);
   fChain->SetBranchAddress("zdc_ZdcModuleMask", &zdc_ZdcModuleMask, &b_zdc_ZdcModuleMask);
   fChain->SetBranchAddress("zdc_VisTruthEnergy", zdc_VisTruthEnergy, &b_zdc_VisTruthEnergy);
   fChain->SetBranchAddress("zdc_EMTruthEnergy", zdc_EMTruthEnergy, &b_zdc_EMTruthEnergy);
   fChain->SetBranchAddress("zdc_TotalTruthEnergy", zdc_TotalTruthEnergy, &b_zdc_TotalTruthEnergy);
   fChain->SetBranchAddress("zdc_NonEMTruthEnergy", zdc_NonEMTruthEnergy, &b_zdc_NonEMTruthEnergy);
   fChain->SetBranchAddress("zdc_VisTruthEnergySum", zdc_VisTruthEnergySum, &b_zdc_VisTruthEnergySum);
   fChain->SetBranchAddress("zdc_EMTruthEnergySum", zdc_EMTruthEnergySum, &b_zdc_EMTruthEnergySum);
   fChain->SetBranchAddress("zdc_TotalTruthEnergySum", zdc_TotalTruthEnergySum, &b_zdc_TotalTruthEnergySum);
   fChain->SetBranchAddress("zdc_NonEMTruthEnergySum", zdc_NonEMTruthEnergySum, &b_zdc_NonEMTruthEnergySum);
   fChain->SetBranchAddress("zdc_ZdcModuleAmp", zdc_ZdcModuleAmp, &b_zdc_ZdcModuleAmp);
   fChain->SetBranchAddress("zdc_ZdcModuleTime", zdc_ZdcModuleTime, &b_zdc_ZdcModuleTime);
   fChain->SetBranchAddress("zdc_ZdcModuleFitAmp", zdc_ZdcModuleFitAmp, &b_zdc_ZdcModuleFitAmp);
   fChain->SetBranchAddress("zdc_ZdcModuleFitT0", zdc_ZdcModuleFitT0, &b_zdc_ZdcModuleFitT0);
   fChain->SetBranchAddress("zdc_ZdcModuleStatus", zdc_ZdcModuleStatus, &b_zdc_ZdcModuleStatus);
   fChain->SetBranchAddress("zdc_ZdcModuleChisq", zdc_ZdcModuleChisq, &b_zdc_ZdcModuleChisq);
   fChain->SetBranchAddress("zdc_ZdcModuleCalibAmp", zdc_ZdcModuleCalibAmp, &b_zdc_ZdcModuleCalibAmp);
   fChain->SetBranchAddress("zdc_ZdcModuleCalibTime", zdc_ZdcModuleCalibTime, &b_zdc_ZdcModuleCalibTime);
   fChain->SetBranchAddress("zdc_ZdcModuleBkgdMaxFraction", zdc_ZdcModuleBkgdMaxFraction, &b_zdc_ZdcModuleBkgdMaxFraction);
   fChain->SetBranchAddress("zdc_ZdcModuleAmpError", zdc_ZdcModuleAmpError, &b_zdc_ZdcModuleAmpError);
   fChain->SetBranchAddress("zdc_ZdcModuleMinDeriv2nd", zdc_ZdcModuleMinDeriv2nd, &b_zdc_ZdcModuleMinDeriv2nd);
   fChain->SetBranchAddress("zdc_ZdcModulePresample", zdc_ZdcModulePresample, &b_zdc_ZdcModulePresample);
   fChain->SetBranchAddress("zdc_ZdcModulePreSampleAmp", zdc_ZdcModulePreSampleAmp, &b_zdc_ZdcModulePreSamplemp);
   Notify();
}

Bool_t NtupleReader_mc::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void NtupleReader_mc::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t NtupleReader_mc::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef NtupleReader_mc_cxx
