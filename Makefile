CXX      = g++
CXXFLAGS = -Wall `root-config --cflags` -g -o2
LDFLAGS  = -Wall `root-config --glibs` -Wl,-rpath=$(ROOUNFOLDDIR)


INCLUDEDIRS = -I$(ROOUNFOLDDIR)/src -I./utils -I./Input

LIBDIRS     = -L$(ROOUNFOLDDIR)
LIBS        = -lRooUnfold

OBJ1        = obj/mcAnalysis.o
SOURCE1     = src/mcAnalysis.C

#CSOURCES    = $(addprefix $(DSOURCES),$(SOURCES))
#CINCLUDES   = $(addprefix $(DINCLUDES),$(INCLUDES))

all: mcAnalysis

mcAnalysis: $(SOURCE1)
	$(CXX) $(CXXFLAGS) $(SOURCE1) $(INCLUDEDIRS) \
	$(LIBDIRS) $(LIBS) $(LDFLAGS) -o $(OBJ1)

clean :
	rm $(OBJ1) 
